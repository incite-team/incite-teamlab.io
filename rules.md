---
layout: default
title: Rules
permalink: /rules/
---

## Rules

What hacker likes rules? `STFU || GTFO`

* you must participate to remain a member.
* there is no recording or filming allowed.
* what happens at the meetings, stays at meetings.
* keep your talk under 10 mins.
* buy a 0xcafe or something.