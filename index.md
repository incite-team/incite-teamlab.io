---
layout: default
permalink: /
---

```txt
   )                |                                ...            _                    ( 
  (_,________ _     |.===.       `  _ ,  '      o,*,(o o)         _|_|_       _ ________,_)
    '----)==)`<     {}o o{}     -  (o)o)  -    8(o o)(_)Ooo       (o o)       >`(==(----'  
         `~~\__) ooO--( )--Ooo--ooO'( )--Ooo-ooO-( )---Ooo----ooO--( )--Ooo- (__/~~`       
```

We are a loose group of hackers and professional security researchers in Mexico City that focused on learning and applying technical security research. We also occasionally eat tacos, sip cold brews and drink beer together.

This is a spin-off from [Source Incite](https://srcincite.io/) and was developed to help drive digital security research in Mexico City.

We have monthly meetings that offer short 5-10 minute presentations about a large range of topics including reversing, pentesting, electronics, programming, and general hacking.

Please see the [about]({{ "/about" | prepend: site.baseurl }}) page for more information on our meetings.

### How to join Incite Team

There is no formal membership, however, participation is mandatory:

* read the [about]({{ "/about" | prepend: site.baseurl }}) page
* follow the ~~white rabbit~~ [rules]({{ "/rules" | prepend: site.baseurl }}) 
* come to a meeting
* join us on ~~irc~~ slack #asdf
