---
layout: default
title: About
permalink: /about/
---

## Who

We are a loose group of hackers and professional security researchers in Mexico City that are focused on learning and applying technical security research. We also occasionally eat tacos, sip cold brews and drink beer together.

## Our meetings

The meetings consist of:

* short "turbo-talks"
* ctf's and technical challenges
* bug bounties
* etc

The short "turbo-talks" are really, short as in, anywhere from 5 to 10 minutes. I am working on getting a new projector for us, sit tight.

We also have a byob (bring your own bug), which can be your bug, (n-day whateva) or some other one that your are interested in researching and exploiting. In fact, it can even be a bug in your own code that you want to fix! :->

For time/location of meetings please see our [meetings]({{ "/meetings" | prepend: site.baseurl }}) page. Before attending, please see the Rules page. If this is your first time, come prepared to speak.