---
layout: default
title: Meetings
permalink: /meetings/
---

## Meetings

Check when the next meetup is! We meet upstairs at Starbucks:

```Avenida Tamaulipas 55, Hipódromo, 06140 Ciudad de México, CDMX```

<style>
.fc-today {
    background: #666 !important;
    border: none !important;
    border-top: 1px solid #ddd !important;
    font-weight: bold;
} 

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="{{ "/scripts/moment.min.js" | prepend: site.baseurl }}" ></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.2.0/fullcalendar.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.2.0/fullcalendar.css">
<link rel="stylesheet" media="print" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.2.0/fullcalendar.css">

<script>
$(document).ready(function() {
    $('#calendar').fullCalendar({
        header: {
            right: 'prev,next',
        },
        displayEventTime: true,
        timeFormat: 'h(:mm)a',
        eventColor: '#b5e853',
        eventTextColor: '#151515',
        /* 
            https://www.raymondcamden.com/2017/02/24/an-example-of-a-static-site-with-a-dynamic-calendar
            generate the date: date -v1d -v+1m -v-1d -v+tue "+%Y-%m-%d"
        */
        events: [ 
            { "title":"meetup", "description": "meetup", "start": "2018-11-05T19:00:00"},
            { "title":"meetup", "description": "meetup", "start": "2018-12-04T19:00:00"},
            { "title":"meetup", "description": "meetup", "start": "2019-01-08T19:00:00"},
            { "title":"meetup", "description": "meetup", "start": "2019-02-05T19:00:00"},
            { "title":"meetup", "description": "meetup", "start": "2019-03-05T19:00:00"},
            { "title":"meetup", "description": "meetup", "start": "2019-04-08T19:00:00"},
            { "title":"meetup", "description": "meetup", "start": "2019-05-07T19:00:00"},
            { "title":"meetup", "description": "meetup", "start": "2019-06-04T19:00:00"},
        ],
        eventRender: function(event, element, view) {
        }

    });
});
</script>
<div id="calendar" style="width:80%"></div>
